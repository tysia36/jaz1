package credits;

import static java.lang.Math.*;

public class ConstantCredit {
	
	private double instalment;
	private double sum;
	private double capital;
	private double numberOfInstalments;
	private double percent;
	private double factor;
	
	public ConstantCredit(double capital, double numberOfInstalments,
						  double percent)
	{
		this.capital=capital;
		this.numberOfInstalments=numberOfInstalments;
		this.percent=percent;
	}
	
	private void calculateFactor()
	{
		this.factor=1.0+(this.percent/(12.0*100.0));
	}
	
	private double calculateInstalment()
	{
		this.calculateFactor();
		this.instalment=this.capital*pow(this.factor,this.numberOfInstalments)*
				((this.factor-1.0)/(pow(this.factor,this.numberOfInstalments)-1.0));
		
		return this.instalment;
	}
	
	private void calculateSum()
	{
		this.calculateInstalment();
		this.sum=this.instalment*this.numberOfInstalments;
	}
	
	public double getInstalment() {
		this.calculateInstalment();
		return instalment;
	}
	
	public double getSum() {
		this.calculateSum();
		return sum;
	}
	
	public double getCapital() {
		return capital;
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	public double getNumberOfInstalments() {
		return numberOfInstalments;
	}
	public void setNumberOfInstalments(double numberOfInstalments) {
		this.numberOfInstalments = numberOfInstalments;
	}
	public double getPercent() {
		return percent;
	}
	public void setPercent(double percent) {
		this.percent = percent;
	}
	public double getFactor() {
		this.calculateFactor();
		return factor;
	}
	
	
	

}