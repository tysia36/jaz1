package decision;

import credits.ConstantCredit;
import credits.DiminishingCredit;

public class DecideAndPrepare 
{
	private double capital;
	private double numberOfInstalments;
	private double percent;
	
	private String typeOfCredit;
	
	public DecideAndPrepare(String credit, double capital, double numberOfInstalments,
			  double percent)
	{
			this.capital=capital;
			this.numberOfInstalments=numberOfInstalments;
			this.percent=percent;
			if (credit.equals("diminishing"))
				this.typeOfCredit="kredyt malej�cy";
			else
				this.typeOfCredit="kredyt sta�y";

	}
	
	public String prepare()
	{
		String toReturn;
		if (this.typeOfCredit.equals("kredyt malej�cy"))
		{
			DiminishingCredit credit2=new DiminishingCredit(this.capital,
					this.numberOfInstalments,this.percent);
			double help,help2;
			int i=1;
			toReturn="<h1>Prezentujemy wybrany przez Ciebie  "
					+this.typeOfCredit
					+".</h1>";
			toReturn=toReturn+"<table><tr><td><b>Po�yczono(kapita�)"
					+ "</b></td><td>"
					+String.format( "%.2f", this.capital )
					+"</td></tr><tr><td><b>Procent"
					+ "</b></td><td>"
					+String.format( "%.2f", this.percent )
					+"</td></tr><tr><td><b>Ilo�� rat"
					+ "</b></td><td>"
					+String.format( "%.2f", this.numberOfInstalments )
					+ "</td></tr></table><table>\n";
			toReturn= toReturn+"<tr><td><b>Nr raty</b></td>"
					+ "<td><b>Rata kapita�owa</b></td>"
					+ "<td><b>Rata odsetkowa</b></td>"
					+ "<td><b>Ca�kowita rata</b></td></tr>\n";

			while (i<=this.numberOfInstalments)
			{
				help=credit2.getCapitalInstalment();
				help2=credit2.calculateInstalment(i);
				toReturn=toReturn+"<tr><td>"
					+i
					+"</td><td>"
					+String.format( "%.2f", help )
					+" z�.</td><td>"
					+String.format( "%.2f", help2 )
					+" z�.</td><td>"
					+String.format( "%.2f", help+help2 )
					+" z�.</td><td></tr>\n";
				i++;
			}
			help=credit2.getSum();
			toReturn=toReturn+"</table>"
					+ "<table><tr><td> W sumie do sp�aty</td><td> "
					+String.format( "%.2f", help )
					+"</td></tr></table>\n";
		}
		else
		{
		
			ConstantCredit credit1=new ConstantCredit(this.capital,
								this.numberOfInstalments,this.percent);
			double help;
			int i=1;
			toReturn="<h1>Prezentujemy wybrany przez Ciebie  "
					+this.typeOfCredit
					+".</h1>";
			toReturn=toReturn+"<table><tr><td><b>Po�yczono(kapita�)"
				+ "</b></td><td>"
				+String.format( "%.2f", this.capital )
				+"</td></tr><tr><td><b>Procent"
				+ "</b></td><td>"
				+String.format( "%.2f", this.percent )
				+"</td></tr><tr><td><b>Ilo�� rat"
				+ "</b></td><td>"
				+String.format( "%.2f", this.numberOfInstalments )
				+ "</td></tr></table><table>\n";
			toReturn=toReturn+"<tr><td><b>Nr raty</b></td>"
				+ "<td><b>Rata</b></td></tr>\n";
			while (i<=this.numberOfInstalments)
			{
				help=credit1.getInstalment();
				toReturn=toReturn+"<tr><td>"
					+i
					+"</td><td>"
					+String.format( "%.2f", help )
					+" z�.</td></tr>\n";
				i++;
			}
			help=credit1.getSum();
			toReturn=toReturn+"</table>"
				+ "<table><tr><td> W sumie do sp�aty</td><td> "
				+String.format( "%.2f", help )
				+"</td></tr></table>\n";
		}
		return toReturn;
	}
	

	public double getCapital() {
		return capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public double getNumberOfInstalments() {
		return numberOfInstalments;
	}

	public void setNumberOfInstalments(double numberOfInstalments) {
		this.numberOfInstalments = numberOfInstalments;
	}

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}

	public String getTypeOfCredit() {
		return typeOfCredit;
	}

	public void setTypeOfCredit(String typeOfCredit) {
		this.typeOfCredit = typeOfCredit;
	}
	
	
}
