package servlets;
import java.io.IOException;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import decision.DecideAndPrepare;


@WebServlet("/results")
public class ResultsServlet extends HttpServlet{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 
	 */
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
			response.sendRedirect("/");
		}
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
			String credit = request.getParameter("credit");
			String capital = request.getParameter("capital");
			String numberOfInstalments = request.getParameter("numberOfInstalments");
			String percent = request.getParameter("percent");
			
			double doubleCapital = Double.parseDouble(capital);
			double doubleNumberOfInstalments = Double.parseDouble(numberOfInstalments);
			double doublePercent = Double.parseDouble(percent);
			
			response.setContentType("text/html");
			
			String writing;
			
			DecideAndPrepare toWrite= new DecideAndPrepare(credit, doubleCapital, doubleNumberOfInstalments, doublePercent);
			writing = toWrite.prepare();
			
			response.getWriter().print(writing);
		}

}
