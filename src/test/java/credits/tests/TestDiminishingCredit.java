package credits.tests;

import static org.junit.Assert.*;

import java.io.IOException;


import org.junit.Test;
import java.util.List;
import java.util.ArrayList;

import credits.DiminishingCredit;

public class TestDiminishingCredit {

	@Test
	public void testThatConstructorWorks() throws IOException  {
		DiminishingCredit credit = new DiminishingCredit(1000,10,4);
		
		List<Double> list = new ArrayList<Double>();
		list.add(credit.getCapital());
		list.add(credit.getNumberOfInstalments());
		list.add(credit.getPercent());
		list.add(credit.getCapitalInstalment());
		list.add(Math.floor(credit.getPercentInstalment() * 100) / 100);

		List<Double> anotherList = new ArrayList<Double>();
		anotherList.add(1000.0);
		anotherList.add(10.0);
		anotherList.add(4.0);
		anotherList.add(100.0);
		anotherList.add(3.33);
		
		assertEquals(anotherList, list);
		
	}
	
	@Test
	public void testThatCalculatingInstalmentWorks() throws IOException{
		DiminishingCredit credit = new DiminishingCredit(1000,10,4);
		double instalment = credit.calculateInstalment(5);
		
		assertEquals(2.0,instalment,0.01);
	}
	
	@Test
	public void testThatCalculatingAllCostsWorks() throws IOException{
		DiminishingCredit credit = new DiminishingCredit(1000,10,4);
		double sum = credit.getSum();
		assertEquals(1018.33,sum,0.01);
	}
	
}
