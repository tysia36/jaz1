package credits.tests;

import static org.junit.Assert.*;

import java.io.IOException;


import org.junit.Test;
import java.util.List;
import java.util.ArrayList;

import credits.ConstantCredit;

public class TestConstantCredit {
	
	@Test
	public void testThatConstructorWorks() throws IOException  {
		ConstantCredit credit = new ConstantCredit(1000,12,4);
		
		List<Double> list = new ArrayList<Double>();
		list.add(credit.getCapital());
		list.add(credit.getNumberOfInstalments());
		list.add(credit.getPercent());
		
		List<Double> anotherList = new ArrayList<Double>();
		anotherList.add(1000.0);
		anotherList.add(12.0);
		anotherList.add(4.0);
		
		assertEquals(anotherList, list);
		
	}
	
	@Test
	public void testThatCalculatingFactorWorks() throws IOException{
		ConstantCredit credit = new ConstantCredit(1000,12,4);
		double factor = credit.getFactor();
		double shouldBeFactor=1.0+(4.0/(12.0*100.0));
		
		assertEquals(shouldBeFactor,factor,0.0001);
	}
	
	@Test
	public void testThatCalculatingInstalmentWorks() throws IOException{
		ConstantCredit credit = new ConstantCredit(1000,12,4);
		double instalment = credit.getInstalment();
		
		assertEquals(85.15,instalment,0.01);
	}
	
	@Test
	public void testThatCalculatingAllCostsWorks() throws IOException{
		ConstantCredit credit = new ConstantCredit(1000,12,4);
		double sum = credit.getSum();
		assertEquals(1021.8,sum,0.01);
	}

}
