package prepare.tests;

import static org.junit.Assert.*;

import java.io.IOException;


import org.junit.Test;

import decision.DecideAndPrepare;

public class TestDecideAndPrepare {
	
	@Test
	public void testThatGoodDecisionIsMadeWhenCreditIsDiminishing() throws IOException  {
		
		DecideAndPrepare decision= new DecideAndPrepare("diminishing", 1000.0, 12.0,4.0);
		
		String decisionMade= decision.getTypeOfCredit();
		String decisionShouldBeMade = "kredyt malej�cy";
		
		assertEquals(decisionMade,decisionShouldBeMade);
		
	}
	
	@Test
	public void testThatGoodDecisionIsMadeWhenCreditIsConstant() throws IOException  {
		
		DecideAndPrepare decision= new DecideAndPrepare("constant", 1000.0, 12.0,4.0);
		
		String decisionMade= decision.getTypeOfCredit();
		String decisionShouldBeMade = "kredyt sta�y";
		
		assertEquals(decisionShouldBeMade, decisionMade);
		
	}
	
	@Test
	public void testThatPrepareReturnsRightString() throws IOException  {
		
		DecideAndPrepare decision= new DecideAndPrepare("constant", 100.0,6.0,2.0);
		
		String isReturning=decision.prepare();
		String shouldReturn="<h1>Prezentujemy wybrany przez Ciebie  kredyt sta�y.</h1>"
				+"<table><tr><td><b>Po�yczono(kapita�)"
				+ "</b></td><td>100,00"
				+"</td></tr><tr><td><b>Procent"
				+ "</b></td><td>2,00"
				+"</td></tr><tr><td><b>Ilo�� rat"
				+ "</b></td><td>6,00"
				+ "</td></tr></table><table>\n"
				+"<tr><td><b>Nr raty</b></td>"
				+ "<td><b>Rata</b></td></tr>\n"
				+"<tr><td>1</td><td>16,76 z�.</td></tr>\n"
				+"<tr><td>2</td><td>16,76 z�.</td></tr>\n"
				+"<tr><td>3</td><td>16,76 z�.</td></tr>\n"
				+"<tr><td>4</td><td>16,76 z�.</td></tr>\n"
				+"<tr><td>5</td><td>16,76 z�.</td></tr>\n"
				+"<tr><td>6</td><td>16,76 z�.</td></tr>\n"
				+"</table>"
				+ "<table><tr><td> W sumie do sp�aty</td><td> 100,58"
				+"</td></tr></table>\n";
		
		assertEquals(shouldReturn,isReturning);
		
	}

}
